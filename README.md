# Project Library
This is a simple library project which aims to teach the user how to combine some technologies like Message Queuing and 
Elastic-Search with a Django application. In order to achieve this, we made the project very simple and did not take
into account some aspects of normal applications. For example, there is no registration procedure in the app and 
creating model objects is done in the admin panel by the superuser. We did not even change the settings for a production version, which
simply means that the Debug parameter is set to True, there is no need to do 'collectstatic', no need to set the 
'allowed_hosts' parameter in the settings, etc.

### Technologies
- Django (version: 2.2.8 with python 3)
- Elastic-Search
- Celery
- RabbitMQ
- MySQL
- MongoDB

### Project Description
There is a list of Books and Authors in the app. Every week on Monday at 5:00 PM, an Email or SMS
is sent to all the authors with the following conditions:
- Each author receives an email/sms containing the list of 5 latest Books that have been added to the app for each genre (each book has a genre).
- Each author only receives the books related to the genres that the author himself has already written a book about.
- The list should not contain the books of the author himself (only lists the books that other authors have written). 
### Installation Process
- create a virtualenv called .venv and activate it.
- ```sudo apt-get install gcc libmysqlclient-dev python3-dev build-essential libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev```
- ```pip install -r requirements.txt```
- install Erlang and RabbitMQ server
- install Mysql and create a database called library(default username and password in the application's settings.py are set to lib & lib respectively)
- ```python manage.py migrate```
- After you migrated the database, you can pre-populate it with some data. We have prepared a sample sql file which populates 
the 'authors' and 'books' tables with some dummy data. The file is in the root directory and is named 'prepopulate.sql'. Source this file in your database with the following command:
```
mysql> source prepopulate.sql
```
- install MongoDB
- Run celery with ```celery -A library worker -l info```
- You can open another terminal and (after activating virtualenv) run ```python manage.py send_updates_to_authors```
- It works! Now if you want to create a cronjob to do this every week (as the project's description says), open crontab and create a cronjob. For your convenience, I have added a sample cronjob in the 'cron_sample' file so you can use it. (the file is in txt format). 
It triggers the file 'send_updates_to_authors.sh' which in turn will run the management command that we ran earlier!
### Important Notes
- Make sure the file 'send_updates_to_authors.sh' is executable so that your cronjob can run it!

- Each book has a Genre (only 1, for simplicity). The list of acceptable genres is defined in the settings file. You can add or remove items to it at your convenience. 
