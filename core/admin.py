from django.contrib import admin
from .models import Author, Book


class BookAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'genre',
        'date_added',
        'author',
    )


class AuthorAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'email',
        'mobile',
    )


admin.site.register(Book, BookAdmin)
admin.site.register(Author, AuthorAdmin)
