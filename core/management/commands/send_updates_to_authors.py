from django.core.management import BaseCommand
from core.tasks import send_updates


class Command(BaseCommand):
    help = 'Sends an update sms/email to all authors; Which is a list of 5 new books that have recently been added' \
           ' to the library in each genre (specific to each author).'

    def handle(self, *args, **options):
        send_updates.delay()
        print("Job sent to celery successfully!")
