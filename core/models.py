from django.db import models
from library import settings


class Author(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    mobile = models.CharField(max_length=12)

    class Meta:
        db_table = 'authors'
        verbose_name = 'author'
        verbose_name_plural = 'authors'
        unique_together = (('name', 'email', 'mobile'),)

    def __str__(self):
        return self.name


class Book(models.Model):
    genre_options = settings.GENRES

    title = models.CharField(max_length=255)
    genre = models.IntegerField(choices=genre_options, default=0)
    date_added = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(to=Author, on_delete=models.CASCADE, related_name='books')

    class Meta:
        db_table = 'books'
        verbose_name = 'book'
        verbose_name_plural = 'books'
        unique_together = (('title', 'genre', 'author'),)

    def __str__(self):
        return self.title
