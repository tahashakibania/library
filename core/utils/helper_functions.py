from .mongo_entities import BookGroup, Book, Recipient, UpdateLog


def group_books(books, genre):
    return BookGroup(
        genre=genre,
        books=_serialize_books(books)
    )


def _serialize_books(books):
    serialized_books = []
    for book in books:
        serialized_books.append(Book(
            book_id=book.id,
            title=book.title,
            author_name=book.author.name,
            date_added_to_library=book.date_added
        ))

    return serialized_books


def serialize_author(author):
    serialized_data = Recipient(
        author_id=author.id,
        name=author.name,
        email=author.email,
        mobile=author.mobile
    )

    return serialized_data


def generate_update_data(book_groups, recipient, send_method, subject):
    data = UpdateLog(
        recipient=recipient,
        sent_via=send_method,
        subject=subject,
        book_groups=book_groups
    )

    return data
