from mongoengine import Document, DateTimeField, IntField, StringField, EmailField, ListField, \
    EmbeddedDocument, EmbeddedDocumentField
import datetime


class Recipient(EmbeddedDocument):
    author_id = IntField()
    name = StringField(max_length=255)
    email = EmailField(max_length=255)
    mobile = StringField(max_length=12)


class Book(EmbeddedDocument):
    book_id = IntField()
    title = StringField(max_length=255)
    author_name = StringField(max_length=255)
    date_added_to_library = DateTimeField()


class BookGroup(EmbeddedDocument):
    genre = StringField(max_length=100)
    books = ListField(EmbeddedDocumentField(Book))


class UpdateLog(Document):
    date = DateTimeField(default=datetime.datetime.now)
    recipient = EmbeddedDocumentField(Recipient)
    subject = StringField(max_length=255)
    sent_via = StringField(max_length=50)
    book_groups = ListField(EmbeddedDocumentField(BookGroup))
