from celery import shared_task
from core.models import Author, Book
from core.utils.helper_functions import group_books, serialize_author, generate_update_data
from core.utils.send_methods import Email
from mongoengine import connect, disconnect
from library import settings


@shared_task
def send_updates():
    try:
        authors = Author.objects.all()
        for author in authors:
            author_genres = []
            author_books = author.books.all()
            for book in author_books:
                author_genres.append(book.genre)

            author_genres = set(author_genres)

            book_groups = []
            for genre in author_genres:
                books = Book.objects.filter(genre=genre).exclude(author=author).order_by('-date_added')[:5]
                book_groups.append(group_books(books, dict(settings.GENRES).get(genre)))

            recipient = serialize_author(author)

            update_data = generate_update_data(book_groups, recipient, 'Email', 'Your weekly suggested books!')

            Email().send_email(update_data)

            # Save the update data in MongoDB
            connect('library_app')

            update_data.save()
            print("data saved to MongoDB successfully!")

            disconnect()

        return "Celery task completed successfully!"

    except Exception as e:
        return "Exception occurred during the execution of celery task: {}".format(str(e))
