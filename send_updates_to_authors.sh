#!/bin/bash

cwd=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd ${cwd}
source .venv/bin/activate
python manage.py send_updates_to_authors